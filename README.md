
# Running SA In Istio

This note describes how to run sa on minikube k8s.

## Requirements

Requires VirtualBox at least version 5.2.16

Requires a recent version of minikube.  The scripts directory contains run-minikube.sh which will start up
minikube with the required parameters for istio.

Tested against istio 1.0.3 which should be installed on the host machine.

## Port Forwarding

I have been running with port forwarding configured like:

![port forwarding](img/port-forwards.png)

## Adding istio rbac



## Proxying

Because of the way a browser encodes the Hosts field we need to proxy the public port using socat like:

	sudo socat TCP-LISTEN:80,fork TCP:<linux-host-ip>:3084

Add -v option to inspect all the traffic but there is a lot of javascript output, its better to use the browser option-command i t inspect the network traffic.


## TLS

### Clone Cert Manager

    git clone https://github.com/jetstack/cert-manager.git

### Install Cert Manager

    kubectl apply -f contrib/manifests/cert-manager/with-rbac.yaml
